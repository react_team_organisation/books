import React, {Component} from 'react';
import {connect} from 'react-redux';


class BookDetail extends Component {

    render() {
        if (!this.props.activeBook) {
            return <div>Select a book to get started.</div>
        }

        return (
            <div>
                <div>
                    Details for:  {this.props.activeBook.bookSelected.title}
                </div>
                <div>
                    Pages:  {this.props.activeBook.bookSelected.pages}
                </div>
            </div>)
    }
}

function mapStateToProps(state) {
    return {
        activeBook: state.activeBook,
    }

}

export default connect(mapStateToProps)(BookDetail);