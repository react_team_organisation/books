import React from 'react';
import BookList from '../containers/bookList';
import BookDetail from '../containers/bookDetail';
const App = () => {
    return (
        <div>
            <BookList/>
            <BookDetail/>
        </div>
    )
};
export default App;