# BookList dans organisation react_team_roganisation

### Getting Started

There are two methods for getting started with this repo.

#### Familiar with Git?
Checkout this repo, install dependencies, then start the gulp process with the following:

```
> git clone https://mjaziri@bitbucket.org/react_team_organisation/book_list.git
> cd book_list
> npm install
> npm start
```
